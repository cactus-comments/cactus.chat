---
title: "Self-Hosting"
description: "How to self-host the Cactus Comments server?"
lead: ""
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "server"
weight: 32
toc: true
---

## Prerequisites

You need to run your own Matrix homeserver.


## Configuration

### Synapse

Create an appservice registration file `cactus.yaml` that Synapse can read. Use the following template and change the tokens:

```yaml
# A unique, user-defined ID of the application service which will never change.
id: "Cactus Comments"

# Where the cactus-appservice is hosted:
url: "http://cactus:5000"

# Unique tokens used to authenticate requests between our service and the
# homeserver (and the other way). Use the sha256 hashes of something random.
# CHANGE THESE VALUES.
as_token: "a2d7789eedb3c5076af0864f4af7bef77b1f250ac4e454c373c806876e939cca"
hs_token: "b3b05236568ab46f0d98a978936c514eac93d8f90e6d5cd3895b3db5bb8d788b"

# User associated with our service. In this case "@cactusbot:example.com"
sender_localpart: "cactusbot"

namespaces:
  aliases:
    - exclusive: true
      regex: "#comments_.*"
```

Then add the file path to your Synapse `homeserver.yaml`:

``` yaml
app_service_config_files:
  - "/path/to/cactus.yaml"
```

To allow guest comments without users needing to log in, you need to have guest registration enabled. To do this set `allow_guest_access: true` in `homeserver.yaml`.

### Cactus Comments application service

The Cactus Comments application service is entirely configured with environment variables.

| Name                      | Required? | Description                                                                                                                                                                    |
|---------------------------|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CACTUS_HS_TOKEN           | Yes       | The token used to authenticate the homeserver. This should match `hs_token` in the registration YAML file.                                                                     |
| CACTUS_AS_TOKEN           | Yes       | The token used to authenticate the application service to the homeserver. This should match  `as_token` in the registration YAML file.                                         |
| CACTUS_HOMESERVER_URL     | Yes       | The complete URL of the homeserver that the application service is registered with.                                                                                            |
| CACTUS_USER_ID            | Yes       | The user id for the chatbot interface. This should be based on `sender_localpart` from the registration YAML file. Example: `@cactusbot:example.com`.                          |
| CACTUS_NAMESPACE_REGEX    | No        | The regex for room aliases that the application service controls. This should match the regex in the registration YAML file. Example: `#_comments2_.*`.                        |
| CACTUS_NAMESPACE_PREFIX   | No        | The prefix of room aliases that the application service controls. This should only be set if `CACTUS_NAMESPACE_REGEX` is set and it should match that. Example: `_comments_2`. |
| CACTUS_REGISTRATION_REGEX | No        | Only allow user id's matching this regex to register new sites. Example: `@user1:example.org|@user2:example.org`                                                               |

`CACTUS_NAMESPACE_REGEX` and `CACTUS_NAMESPACE_PREFIX` exists in case you want to change the room alias regex that the application service controls. This is useful if you want to run multiple Cactus Comments deployments on the same homeserver.

There are four required environment variables and they should match those defined in the `cactus.yaml` above:

```yaml
CACTUS_HS_TOKEN=b3b05236568ab46f0d98a978936c514eac93d8f90e6d5cd3895b3db5bb8d788b
CACTUS_AS_TOKEN=a2d7789eedb3c5076af0864f4af7bef77b1f250ac4e454c373c806876e939cca
CACTUS_HOMESERVER_URL=http://synapse:8008
CACTUS_USER_ID=@cactusbot:example.com
```

## Running cactus-appservice

Currently, `cactus-appservice` is primarily [distributed as a docker image](https://hub.docker.com/r/cactuscomments/cactus-appservice).
However, you can opt to run it as a systemd service by cloning the repo, at the expense of some additional setup steps.

The application service is stateless either way.

### Using docker

If the environment variables above were defined in `cactus.env`, you could start the application service directly with docker like:

```bash
$ docker run --env-file cactus.env --name cactus cactuscomments/cactus-appservice:latest
```

In `docker-compose`, this service might look like:

```yaml
services:
  cactus:
    image: cactuscomments/cactus-appservice:latest
    env_file: "cactus.env"
```

### Using systemd

See the dedicated instructions for systemd [here](../systemd-service).
