---
title: "Moderation"
description: "Moderating Comment Sections"
lead: "Cactus Comments have a few tools to help with moderation built-in. For bigger needs, the Matrix ecosystem has solutions that can work with Cactus Comments."
date: 2023-09-23T14:04:58+02:00
lastmod: 2023-09-23T14:04:58+02:00
draft: false
images: []
menu:
  docs:
    parent: "getting-started"
weight: 115
---


## Household batteries included

For smaller moderation needs, Cactus Comments have two features:

1) power-level replication
2) ban propagation

Power-level replication means that if you invite and promote someone to the
moderation room, they will have the same power level in all your comment
sections. This can be helpful as a simple mechanism to make multiple people
moderators of your comment sections.

Ban propagation means that if you ban a user in one of your comment sections,
they will automatically be banned in all your comment sections.


## Moderation in larger communities

Cactus Comments exclusively uses standard Matrix events. This means you can
use any moderation tool for moderation in Matrix to moderate your comment
sections.

Take a look at the [official Community Moderation
documentation](https://matrix.org/docs/communities/moderation/).

The wider Matrix community is moving to
[Draupnir](https://github.com/the-draupnir-project/Draupnir) instead of
Mjolnir.


## Approving Comments

A commonly requested feature is the ability to hide comments until they are
approved by a moderator. This is not currently possible in Matrix, the closest
solution (not implemented) being
[MSC3531](https://github.com/matrix-org/matrix-spec-proposals/pull/3531/files).
